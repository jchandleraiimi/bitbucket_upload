SELECT
[catchment], [shortcode], [week_commencing], [weighted_solids], [site_budget_volume]
FROM [S_INSIGHT_REPORTING].[VIEW_DRY_SOLIDS_SITES] WHERE  [shortcode] IN ('BRAKST','SCLAST') AND [week_commencing] = '2018-04-30' ORDER BY [shortcode],[week_commencing];

SELECT [catchment], [shortcode], AVG(PreQ.weighted_solids) As 'avg.solids', AVG(site_budget_volume) As 'site_budget_volume'
FROM	(SELECT [catchment], [shortcode], [week_commencing], [event_start], [total_dur], [dur_x_ms], [weighted_solids], [site_budget_volume]
		FROM [S_INSIGHT_REPORTING].[DRY_SOLIDS_LOADS] WHERE [shortcode] IN ('BRAKST','SCLAST') AND [week_commencing] = '2018-04-30') As PreQ
GROUP BY [catchment], [shortcode];

SELECT
[catchment], [shortcode], [week_commencing], [event_start], [total_dur], [dur_x_ms], [weighted_solids], [site_budget_volume]
FROM [S_INSIGHT_REPORTING].[DRY_SOLIDS_LOADS] WHERE [shortcode] IN ('BRAKST','SCLAST') AND [week_commencing] = '2018-04-30' ORDER BY [shortcode],[week_commencing];

SELECT * FROM [S_INSIGHT_REPORTING].[DRY_SOLIDS_OUTPUT] WHERE [shortcode] IN ('BRAKST','SCLAST') AND [event_date] IN ('2018-04-30','2018-05-01','2018-05-02','2018-05-03','2018-05-04','2018-05-05','2018-05-06') ORDER BY [shortcode], [event_start];

