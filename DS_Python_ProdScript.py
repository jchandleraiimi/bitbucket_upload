# -*- coding: utf-8 -*-
"""
Created on Fri Aug 31 13:42:06 2018

@author: jBeaumont
"""

# IMPORT LIBRARIES
import pandas as pd
import matplotlib.pyplot as plt
import datetime
import numpy as np


#FUNCTIONS
    def make_date(dates):
        return pd.to_datetime(dates, dayfirst = True).apply(lambda x: x.date())
    
    def inverse_zfill(s, x):
        n = len(s)
        if n < x:
            return s + ((x-n) * '0')
        else:
            return s  


# INITIAL DATA LOAD
load_data = pd.read_csv(r'\\Globalinfra.net\\Teams\\AW_CAMSPR_TS\\Global\\RES Performance Team\\Sludge DS\\DS_Python_Import.csv', encoding = "ISO-8859-1")
STC_LOOKUP = pd.read_csv(r'\\Globalinfra.net\\Teams\\AW_CAMSPR_TS\\Global\\RES Performance Team\\Sludge DS\\STC_LIST.csv')
WRC_LOOKUP = pd.read_csv(r'\\Globalinfra.net\\Teams\\AW_CAMSPR_TS\\Global\\RES Performance Team\\Sludge DS\\WRC_LIST.csv')
SITE_LOOKUP = pd.read_csv(r'\\Globalinfra.net\\Teams\\AW_CAMSPR_TS\\Global\\RES Performance Team\\Sludge DS\\SITE_LOOKUPS.csv')


#FILTER OUT OBVIOUS BAD DATA
load_data = load_data[load_data.lat != 'No Reading']


# LOAD/UNLOAD DATA CLEANSING
load_data['event_date'] = make_date(load_data['event_start'])
load_data['adj_lat'] = load_data['lat'].apply(lambda x: x[:5])
load_data['adj_lon'] = load_data['lon'].apply(lambda x: x[:5])
load_data['new_adj_lon'] = load_data['lon'].apply(lambda x: x[:4])
load_data['adj_location'] = load_data['location'].apply(lambda x: str(x)[:20])
load_data['new_lat'] = load_data['lat'].apply(lambda x: x[:-1])
load_data['2nd_lat'] = pd.to_numeric(load_data['new_lat'], errors='coerce')
load_data['Lat Round'] = load_data['2nd_lat'].apply(lambda x: round(x,2))
load_data['new_lon'] = load_data['lon'].apply(lambda x: x[:-1])
load_data['2nd_lon'] = pd.to_numeric(load_data['new_lon'], errors='coerce')
load_data['3rd_lon'] = load_data['2nd_lon'].apply(lambda x: str(round(x,2)))
load_data['lonchar'] = load_data['lon'].apply(lambda x: x[-1:])
load_data['final_lon'] = load_data.apply(lambda x: inverse_zfill(str(x['3rd_lon']),4) + x['lonchar'], axis = 1)
load_data['adj_gridref'] = load_data.apply(lambda x: str(x['Lat Round']) + x['final_lon'] , axis=1)
load_data = load_data.rename(columns={'mean_solids' : 'Mean Solids Collected'})
CLEAN_LOADS_DATA = load_data[['rtu_serial_no','tanker_event_id','site_name','event_date','event_start','event_type','location','duration','adj_gridref','adj_location','Mean Solids Collected']]
CLEAN_LOADS_DATA = CLEAN_LOADS_DATA.rename(columns={'site_name' : 'vehicle_reg'})
CLEAN_LOADS_DATA['vehicle_reg'] = CLEAN_LOADS_DATA['vehicle_reg'].apply(lambda x: x.replace(' ', ''))

# FIXING THE GRID REF OF THE STC LOOKUP - THEN PICK REQUIRED FIELDS
STC_LOOKUP['lonchar'] = STC_LOOKUP['Long'].apply(lambda x: 'E' if x > 0 else 'W')
STC_LOOKUP['final_lat'] = STC_LOOKUP['Lat'].apply(lambda x: str(round(x,2))[:5])
STC_LOOKUP['final_lon'] = STC_LOOKUP.apply(lambda x: inverse_zfill(str(round(np.abs(x['Long']),2)), 4) + x['lonchar'], axis=1)
STC_LOOKUP['adj_gridref'] = STC_LOOKUP.apply(lambda x: x['final_lat'] + x['final_lon'] , axis=1)
STC_LOOKUP = STC_LOOKUP.rename(columns={'Short code' : 'stc_short_code','Site Name' : 'stc_site_name'})
STC_LOOKUP_CLEAN = STC_LOOKUP[['adj_gridref','stc_short_code','stc_site_name']]

# FIXING THE GRID REF OF THE WRC LOOKUP - THEN PICK REQUIRED FIELDS
WRC_LOOKUP['lonchar'] = WRC_LOOKUP['Long'].apply(lambda x: 'E' if x > 0 else 'W')
WRC_LOOKUP['final_lat'] = WRC_LOOKUP['Lat'].apply(lambda x: str(round(x,2))[:5])
WRC_LOOKUP['final_lon'] = WRC_LOOKUP.apply(lambda x: inverse_zfill(str(round(np.abs(x['Long']),2)), 4) + x['lonchar'], axis=1)
WRC_LOOKUP['adj_gridref'] = WRC_LOOKUP.apply(lambda x: x['final_lat'] + x['final_lon'] , axis=1)
WRC_LOOKUP = WRC_LOOKUP.rename(columns={'Short code' : 'wrc_short_code','Site Name' : 'wrc_site_name'})
WRC_LOOKUP_CLEAN = WRC_LOOKUP[['adj_gridref','wrc_short_code','wrc_site_name']]


# Perform first join to STC Data and fill NaN's with Zero's
JOINED_DF1 = pd.merge(CLEAN_LOADS_DATA,STC_LOOKUP_CLEAN, on = 'adj_gridref', how = 'left')
JOINED_DF1['stc_short_code'] = JOINED_DF1['stc_short_code'].fillna(value = 0)
JOINED_DF1['stc_site_name'] = JOINED_DF1['stc_site_name'].fillna(value = 0)

# Perform second join to WRC Data and fill NaN's with Zero's
JOINED_DF2 = pd.merge(JOINED_DF1,WRC_LOOKUP_CLEAN, on = 'adj_gridref', how = 'left')
JOINED_DF2['wrc_short_code'] = JOINED_DF2['wrc_short_code'].fillna(value = 0)
JOINED_DF2['wrc_site_name'] = JOINED_DF2['wrc_site_name'].fillna(value = 0)

# Create a final column for Short Code & Site Name that pics it from the correct lookup table
JOINED_DF2['shortcode'] = JOINED_DF2.apply(lambda x: x['stc_short_code'] if x['stc_short_code'] != 0 else x['wrc_short_code'] if x['wrc_short_code'] != 0 else 0, axis=1)
JOINED_DF2['site_name'] = JOINED_DF2.apply(lambda x: x['stc_site_name'] if x['stc_short_code'] != 0 else x['wrc_site_name'] if x['wrc_short_code'] != 0 else 0, axis=1)

# Adding fields to determine which lookup the site details came from
JOINED_DF2['type'] = JOINED_DF2.apply(lambda x: 'STC' if x['stc_short_code'] != 0 else 'WRC' if x['wrc_short_code'] != 0 else 0, axis=1)
JOINED_DF2['type_long'] =  JOINED_DF2.apply(lambda x: str(x['type']) + str(x['event_type']), axis = 1)
JOINED_DF2['event_day'] = JOINED_DF2['event_date'].apply(lambda x: x.day)

# Trim data columns back down to only the relevant ones
MERGED_DATA = JOINED_DF2[['rtu_serial_no','tanker_event_id','vehicle_reg','event_start','event_date','event_day','event_type','shortcode','site_name','location','type_long','adj_gridref','type','duration','Mean Solids Collected']]
MERGED_DATA = MERGED_DATA.sort_values(['rtu_serial_no', 'tanker_event_id'], ascending=[True, True])
MERGED_DATA = MERGED_DATA.reset_index()


# FLAGGING BAD DATA
MERGED_DATA['bad_data1'] = MERGED_DATA.apply(lambda x: 1 if x['type'] == 0 else 0 , axis=1)
MERGED_DATA['bad_data2'] = MERGED_DATA.apply(lambda x: 1 if x['duration'] < 60 else 0 , axis=1)
MERGED_DATA['bad_data3'] = MERGED_DATA.apply(lambda x: 1 if x['type_long'] == 'STCLoad' else 0 , axis=1)
MERGED_DATA['bad_data4'] = MERGED_DATA.apply(lambda x: 1 if x['type_long'] == 'WRCUnload' else 0 , axis=1)
MERGED_DATA['bad_data'] = MERGED_DATA.apply(lambda x: 1 if (x['bad_data1'] + x['bad_data2'] + x['bad_data3'] + x['bad_data4']) > 0 else 0 , axis=1)
MERGED_DATA['good_data'] = MERGED_DATA.apply(lambda x: 1 if (x['bad_data1'] + x['bad_data2'] + x['bad_data3'] + x['bad_data4']) == 0 else 0 , axis=1)

mapping = MERGED_DATA['bad_data'] == 1
MERGED_DATA.loc[mapping, 'duration'] = 0
MERGED_DATA.loc[mapping, 'Mean Solids Collected'] = 0


# Check next record is an unload event
MERGED_DATA = MERGED_DATA.assign(nr_is_unload = lambda x: (x['event_type'].shift(-1) == 'Unload').astype(int))
# Check next record is an event from the same day
MERGED_DATA = MERGED_DATA.assign(nr_is_sameday = lambda x: (x['event_day'].shift(-1) == x['event_day']).astype(int))
# Check current record is a load event
MERGED_DATA = MERGED_DATA.assign(is_load = lambda x: (x['event_type'] == 'Load').astype(int))
# Check next record is an unload event from the same day
MERGED_DATA['true_load'] = MERGED_DATA.apply(lambda x: 1 if x['nr_is_unload'] + x['nr_is_sameday'] + x['is_load'] + x['good_data'] == 4 else 0, axis=1)

# Take Mean Solids value from the next record
MERGED_DATA = MERGED_DATA.assign(shift_ms = lambda x: x['Mean Solids Collected'].shift(-1))
# Take shortcode from the next record
MERGED_DATA = MERGED_DATA.assign(shift_shortcode = lambda x: x['shortcode'].shift(-1))
# Take site_name from the next record
MERGED_DATA = MERGED_DATA.assign(shift_site_name = lambda x: x['site_name'].shift(-1))
# Take duration from the next record
MERGED_DATA = MERGED_DATA.assign(shift_duration = lambda x: x['duration'].shift(-1))

# Set the newly taken Mean Solids value back to zero if this record isn't a load on the same day as the unload
MERGED_DATA['true_ms'] = MERGED_DATA.apply(lambda x: x['shift_ms'] if x['true_load'] == 1 else 0, axis=1)

# Set the newly taken 'next site details' back to zero if this record isn't a load on the same day as the unload
MERGED_DATA['unload_shortcode'] = MERGED_DATA.apply(lambda x: x['shift_shortcode'] if x['true_load'] == 1 else 0, axis=1)
MERGED_DATA['unload_site_name'] = MERGED_DATA.apply(lambda x: x['shift_site_name'] if x['true_load'] == 1 else 0, axis=1)
MERGED_DATA['dur_x_solids'] = MERGED_DATA.apply(lambda x: x['duration'] * x['Mean Solids Collected'], axis=1)

PRE_GROUPING = MERGED_DATA[['rtu_serial_no','tanker_event_id','vehicle_reg','event_start','event_date','event_type','true_load','shortcode','site_name','duration','Mean Solids Collected','dur_x_solids','good_data']]


# LOOP FOR AGGREGATING UNLOADS

    last_load = None
    my_site = None
    my_sitename = None
    #capture_site = False
    PRE_GROUPING['total_duration'] = 0
    PRE_GROUPING['total_solids'] = 0
    unique_sites = []
    my_date = 0
    load_error = False
    
    # for row in df
    for i in PRE_GROUPING.index:
        # Make we have a last load index (row id)
        if last_load is None:
            # If it it is a true load, capture here in last_load variable
            if PRE_GROUPING['true_load'][i] == 1:
                last_load = i
                my_date = PRE_GROUPING['event_date'][i]
        else:
            # Again, if it is a true load, we need to do something special
            if PRE_GROUPING['true_load'][i] == 1:
                # Make a note of the site from the previous row
                PRE_GROUPING.loc[last_load, 'final_site'] = my_site
                PRE_GROUPING.loc[last_load, 'final_sitename'] = my_sitename
                # Number of unique Sites
                PRE_GROUPING.loc[last_load, 'unique_sites'] = len(unique_sites)
                # Just in case we have two loads in a row, make site n/a
                my_site = "n/a"
                # Reset unique sites
                unique_sites = []
                # This row is a load, so capture this here :)
                last_load = i
                my_date = PRE_GROUPING['event_date'][i]
                # Reset load Error
                load_error = False
            else:
                # First do a check for non-true loads
                if PRE_GROUPING['event_type'][i] == 'Load':
                    load_error = True
                elif not load_error:
                    # This is an unload row
                    # Check same day
                    if (PRE_GROUPING['event_date'][i] == my_date) and (PRE_GROUPING['good_data'][i] == 1):
                        # Capture the site, in case it is the last
                        my_site = PRE_GROUPING['shortcode'][i]
                        my_sitename = PRE_GROUPING['site_name'][i]
                        my_data_check = PRE_GROUPING['good_data'][i]
                        # If not in unique_sites add it
                        if (my_site not in unique_sites) and (my_data_check == 1):
                            unique_sites.append(my_site)
                        # Update the duration and total solids of the previous load
                        PRE_GROUPING.loc[last_load, 'total_duration'] += PRE_GROUPING['duration'][i]
                        PRE_GROUPING.loc[last_load, 'total_solids'] += PRE_GROUPING['dur_x_solids'][i]
            
POST_GROUPING = PRE_GROUPING


# TIDY UP DATAFRAME
SUB_FINAL_DATA = POST_GROUPING[['rtu_serial_no','tanker_event_id','vehicle_reg','event_start','event_type','shortcode','site_name','duration','unique_sites','final_site','final_sitename','total_duration','total_solids','true_load','good_data']]
SUB_FINAL_DATA = SUB_FINAL_DATA.rename(columns={'final_site' : 'unload_shortcode','final_sitename' : 'unload_sitename'})

# ADD WEIGHTED SOLIDS CALCULATION USING THE ADDED DATA FROM THE BIG LOOP
SUB_FINAL_DATA = SUB_FINAL_DATA.assign(weighted_solids = lambda x: (x['total_solids'] / x['total_duration']))

# FILTER OUT HIGH/LOW DURATION LOADS
FINAL_DATA = SUB_FINAL_DATA[SUB_FINAL_DATA.total_solids > 0]
FINAL_DATA = FINAL_DATA[FINAL_DATA.weighted_solids <= 8]
FINAL_DATA['event_date'] = make_date(FINAL_DATA['event_start'])

# REDUCE DATA TO LOAD EVENTS ONLY NOW THE UNLOAD DATA HAS ALSO BEEN APPLIED
FINAL_DATA = FINAL_DATA[FINAL_DATA.event_type == 'Load']

# TIDY DATAFRAME
FINAL_DATA = FINAL_DATA[['rtu_serial_no','tanker_event_id','vehicle_reg','event_date','event_start','event_type','shortcode','site_name','duration','unique_sites','unload_shortcode','unload_sitename','total_duration','total_solids','weighted_solids','true_load','good_data']]
FINAL_DATA = FINAL_DATA.reset_index()

# Perform final join to site lookup table
SITE_LOOKUP = SITE_LOOKUP.rename(columns={'Site Name' : 'shortcode','TM Code' : 'tm_code','Catchment code':'catchment','Process Type':'process_type'})
FINAL_DATA = pd.merge(FINAL_DATA,SITE_LOOKUP, on = 'shortcode', how = 'left')

# TIDY DATAFRAME
FINAL_DATA = FINAL_DATA[['rtu_serial_no','tanker_event_id','vehicle_reg','event_date','event_start','event_type','catchment','tm_code','process_type','shortcode','site_name','duration','unique_sites','unload_shortcode','unload_sitename','total_duration','total_solids','weighted_solids','true_load','good_data']]
FINAL_DATA = FINAL_DATA.reset_index()



# Export to Azure
from sqlalchemy import create_engine

#For Azure DB
azure_connection_string = "mssql+pyodbc://jbeaumont:yKU&iu$w$blwuZ%uDrQO@aws-insights-sqlsrv-01.database.windows.net/aws-insights-sqldb-01?driver=ODBC+Driver+13+for+SQL+Server"
azure_engine = create_engine(azure_connection_string)

#TARGET
target_table_name = 'DRY_SOLIDS_OUTPUT'
target_table_schema = 'S_INSIGHT_REPORTING'
FINAL_DATA.to_sql(name=target_table_name, con=azure_engine, schema=target_table_schema, if_exists='replace', index=False, )


# Export to CSV
FINAL_DATA.to_csv(r"C:\Users\jbeaumont\Desktop\DrySolids.csv")

