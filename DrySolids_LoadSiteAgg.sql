SELECT * FROM S_INSIGHT_REPORTING.DRY_SOLIDS_OUTPUT LEFT JOIN S_INSIGHT_DM.EI_DATE_DIM ON DRY_SOLIDS_OUTPUT.event_date = EI_DATE_DIM.DAY_DATE WHERE shortcode = 'AYLSST' ORDER BY event_date ASC;

SELECT 

	[catchment]
	,[tm_code]
	,shortcode
	,site_name
	,[process_type]
	,CAL_YEAR As 'year'
	,CAL_MONTH_NUM As 'month_num'
	,MONTH_NAME As 'month'
	,WEEK_BEGIN_DATE As 'week_commencing'

	--td_single = SUM(CASE WHEN unique_sites = 1 THEN total_duration END)
	--,ts_single = SUM(CASE WHEN unique_sites = 1 THEN total_solids END)
	,calc_ws_single = CAST(SUM(CASE WHEN unique_sites = 1 THEN total_solids END) / SUM(CASE WHEN unique_sites = 1 THEN total_duration END) as decimal(18,2))
	--,td_multi = SUM(CASE WHEN unique_sites != 1 THEN total_duration END)
	--,ts_multi = SUM(CASE WHEN unique_sites != 1 THEN total_solids END)
	,calc_ws_multi = CAST(SUM(CASE WHEN unique_sites != 1 THEN total_solids END) / SUM(CASE WHEN unique_sites != 1 THEN total_duration END) as decimal(18,2))

	--,volatility_single_vs_multi = CAST(isnull((ABS(SUM(CASE WHEN unique_sites = 1 THEN total_solids END) / SUM(CASE WHEN unique_sites = 1 THEN total_duration END) - SUM(CASE WHEN unique_sites != 1 THEN total_solids END) / SUM(CASE WHEN unique_sites != 1 THEN total_duration END))) / (SUM(CASE WHEN unique_sites = 1 THEN total_solids END) / SUM(CASE WHEN unique_sites = 1 THEN total_duration END)),0) as decimal(18,2)) 
	--,volatility = CAST(SUM(CASE WHEN unique_sites = 1 THEN total_solids END) / SUM(CASE WHEN unique_sites = 1 THEN total_duration END) - SUM(CASE WHEN unique_sites != 1 THEN total_solids END) / SUM(CASE WHEN unique_sites != 1 THEN total_duration END) as decimal(18,2)) 

	,total_weighted_solids = CAST(SUM(total_solids) / SUM(total_duration) as decimal(18,2))
	,vol_from_single = - (CAST((SUM(CASE WHEN unique_sites = 1 THEN total_solids END) / SUM(CASE WHEN unique_sites = 1 THEN total_duration END)) / (SUM(total_solids) / SUM(total_duration)) as decimal(18,2)) - 1)

	--,solids_single = AVG(CASE WHEN unique_sites = 1 THEN weighted_solids END)
	--,solids_multi = AVG(CASE WHEN unique_sites != 1 THEN weighted_solids END)

FROM S_INSIGHT_REPORTING.DRY_SOLIDS_OUTPUT

LEFT JOIN S_INSIGHT_DM.EI_DATE_DIM ON DRY_SOLIDS_OUTPUT.event_date = EI_DATE_DIM.DAY_DATE

WHERE shortcode IN ('AYLSST','KLYNST')

GROUP BY [catchment], [tm_code], shortcode, site_name, [process_type], CAL_YEAR, CAL_MONTH_NUM, MONTH_NAME, WEEK_BEGIN_DATE

